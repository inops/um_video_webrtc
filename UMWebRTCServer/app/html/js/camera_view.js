// 判断是否是IE浏览器
function isIE() {
    if(!!window.ActiveXObject || "ActiveXObject" in window){
        return true;
    } else {
        return false;
    }
}
// 判断是否支持webrtc
function isSupportWebRTC() {
    if (UmVideo.isWebRtcEnable() == true) {
        return true;
    } else {
        return false;
    }
}
// 判断是否是iPhone设备中的Safari浏览器
function isSafari() {
    var u = navigator.userAgent;
    var iOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
    if (iOS) {
        if (u.indexOf("Safari") > -1) {
            return true
        } else {
            return false
        }
    } else {
        return true
    }
}
// 截取location方法
function GetUrlParam(paraName) {
    var url = document.location.toString();
    var arrObj = url.split("?");
    if (arrObj.length > 1) {
        var arrPara = arrObj[1].split("&");
        var arr;
        for (var i = 0; i < arrPara.length; i++) {
            arr = arrPara[i].split("=");
            if (arr != null && arr[0] == paraName) {
                return arr[1];
            }
        }
        return "";
    } else {
        return "";
    }
}
// 做一些判断
function limitAll() {
    if (isIE()) {
        alert("请更换浏览器，建议使用Google Chrome、Firefox等浏览器！");
        // return true;
    }
    if (!isSupportWebRTC()) {
        alert("浏览器不支持本服务！请更换浏览器，建议使用Google Chrome、Firefox等浏览器！");
        // return true;
    }
    if (!isSafari()) {
        alert("请点击右上角“...”使用Safari打开！");
        return true;
    }
}
if (limitAll()) {
    return false;
}
    var roomName,deviceid;
    var hungupBtn = document.querySelector("#hungup");
    var box = document.getElementById("enterBox");
    console.log("UmVideo Version:", UmVideo.version());
    UmVideo.on('userid', function (){
        console.log("my userid:", UmVideo.userid());
        if (roomName) {
            UmVideo.joinRoom(roomName, deviceid);
        } else {
            console.error("RoomName is empty");
        }
    });

    async function hungup(e) {
        try {
            roomName="";
            UmVideo.hungup();
        } catch (e) {
            handleError(e);
        }
    };
    async function joinRoom(e,cid) {
        if (roomName && roomName==e)
        {
            return;
        }
        try {
            roomName = e
            deviceid = cid
            var wsproto = (location.protocol === 'https:') ? 'wss:' : 'ws:';
            var conn = wsproto + '//' + window.location.host + '/signal.ump';
            UmVideo.setup({
                //signalserver: "wss://localhost:4430/signal"  // or
                //signalserver: "ws://localhost:8088/signal"
                signalserver: conn,
                getusermedianow: true,
                remotes: "#remotes_share",
                mediaconf: {
                    video: false,
                    audio: true
                }
            });
        } catch (e) {
            handleError(e);
        }
    };
    var h = GetUrlParam("h"); // h后边跟着的是md5加密后的
    var cid = GetUrlParam("cid"); // cid后边跟着的是设备ID，例如摄像头
    // 存在h的时候直接加入房间
    if (h && cid) {
        if (!isSupportWebRTC()) {
            alert("浏览器不支持本服务！请更换浏览器，建议使用Google Chrome、Firefox等浏览器！");
        } else {
            joinRoom(h,cid);
            box.style.display = "none";
        }
    }
    hungupBtn.addEventListener('click', function (e) {
        hungup(e);
        box.style.display = "block";
        document.getElementById("cl_big_qrcode").style.display = "none"; // 关闭视频中分享的二维码
    });
    // 手机适配
    if ((navigator.userAgent.match(/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i))) {
        document.querySelector(".function_ul").style.width = "300px"
    }

    var reset_btn = document.getElementById("reset_enter");
    reset_btn.addEventListener("click", ()=>{
        joinRoom(h,cid);
        box.style.display = "none";
    })

    var audio_status = true
    var video_status = true
    var voice_status = true
    // 获取mediaconf里边的状态控制显示哪个按钮
    // if (UmVideo.config().mediaconf.audio) {
    //     document.getElementById("control_audio").src = "./img/start_audio.png"
    //     audio_status = true
    // } else {
    //     audio_status = false
    //     document.getElementById("control_audio").src = "./img/ban_audio.png"
    // }

    // 改变语音状态
    // document.getElementById("control_audio").addEventListener("click", ()=>{
    //     if (audio_status) {
    //         UmVideo.setSelfAudio(false);  
    //         document.getElementById("control_audio").src = "./img/ban_audio.png"
    //         audio_status = false
    //     } else {
    //         UmVideo.setSelfAudio(true);  
    //         document.getElementById("control_audio").src = "./img/start_audio.png"
    //         audio_status = true
    //     }
    // })
    // 改变扬声器状态
    document.getElementById("control_voice").addEventListener("click", ()=>{
        if (voice_status) {
            UmVideo.setOtherAudio(false);
            document.getElementById("control_voice").src = "./img/ban_voice.png"
            voice_status = false
        } else {
            UmVideo.setOtherAudio(true);  
            document.getElementById("control_voice").src = "./img/start_voice.png"
            voice_status = true
        }
    })
    // 直接关闭网页或者浏览器触发挂断方法
    window.onbeforeunload = function() {
        hungup();
    };

    // 点击视频中二维码生成分享二维码
    var click_qrcode1;
    var qrcode_btn = document.getElementById("qrcode");
    var click_qrcode = document.getElementById("click_qrcode")
    var cl_big_qrcode = document.getElementById("cl_big_qrcode") // 获取视频中点击分享的二维码盒子
    var url = window.location.href;
    qrcode_btn.addEventListener("click", ()=>{
        click_qrcode.innerHTML = ""; //移除生成的二维码，保证下边生成新的二维码
        if (cl_big_qrcode.style.display == "none") {
            cl_big_qrcode.style.display = "block";
        } else if (cl_big_qrcode.style.display == "block") {
            cl_big_qrcode.style.display = "none"
        }
        let loca_url = window.location.href
        click_qrcode1 = new QRCode(click_qrcode, {
            width: 100,
            height: 100,
            text: loca_url
        }) 
    })

    // Demo
    // UmVideo.setSelfVideo(false);    关闭或开启视频
    // UmVideo.setSelfAudio(false);    关闭或开启音频
    // UmVideo.isWebRtcEnable();       是否支持WebRtc