md dist
md dist\app
md dist\app\html
md dist\app\html\js
md dist\app\html\css
md dist\app\html\img
md dist\app\lua
md dist\bin

cd app\html
cmd /c "npm run build"
move /y dist\*.js js

cd ..\..
xcopy /s /y bin dist\bin
xcopy /s /y app\html\css dist\app\html\css
xcopy /s /y app\html\img dist\app\html\img
xcopy /s /y app\html\video dist\app\html\video
xcopy /y app\html\js\umvideo.min.js dist\app\html\js
xcopy /y app\html\js\NoSleep.min.js dist\app\html\js
xcopy /y app\html\js\camera_list_room.js dist\app\html\js
xcopy /y app\html\js\camera_view_room.js dist\app\html\js
xcopy /y app\html\js\camera_room.js dist\app\html\js
xcopy /y app\html\js\group_call_room.js dist\app\html\js
xcopy /y app\html\js\umroom.js dist\app\html\js
xcopy /y app\html\*.html  dist\app\html
xcopy /y app\html\*.ump  dist\app\html
xcopy /y /s app\lua dist\app\lua
xcopy /y readme.txt dist
xcopy /y run.bat dist
xcopy /y stop.bat dist
